{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ridemap.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "ridemap.fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Calculate hostname
*/}}
{{- define "ridemap.hostname" }}
{{- if (not (empty .Values.config.hostname)) }}
{{- printf .Values.config.hostname }}
{{- else }}
{{- if .Values.ingress.enabled }}
{{- printf (index .Values.ingress.hosts.ridemap 0).name }}
{{- else }}
{{- printf "%s-ridemap" (include "ridemap.fullname" . ) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate certificate
*/}}
{{- define "ridemap.certificate" }}
{{- if (not (empty .Values.ingress.certificate)) }}
{{- else }}
{{- printf "%s-letsencrypt" (include "ridemap.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate base_url
*/}}
{{- define "ridemap.base_url" }}
{{- if (not (empty .Values.config.base_url)) }}
{{- printf .Values.config.base_url }}
{{- else }}
{{- if .Values.ingress.enabled }}
{{- $host := ((empty (include "ridemap.hostname" . )) | (index .Values.ingress.hosts.ridemap 0) (include "ridemap.hostname" . )) }}
{{- $protocol := (.Values.ingress.tls | ternary "https" "http") }}
{{- $path := (eq $host.path "/" | ternary "" $host.path) }}
{{- printf "%s://%s%s" $protocol $host.name $path }}
{{- else }}
{{- if (empty (include "ridemap.hostname" . )) }}
{{- printf "http://%s-ridemap" (include "ridemap.hostname" . ) }}
{{- else }}
{{- printf "http://%s" (include "ridemap.hostname" . ) }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}