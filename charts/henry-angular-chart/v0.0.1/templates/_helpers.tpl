{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "henry-angular-chart.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "henry-angular-chart.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate web certificate
*/}}
{{- define "henry-angular-chart.web-certificate" }}
{{- if (not (empty .Values.ingress.web.certificate)) }}
{{- printf .Values.ingress.web.certificate }}
{{- else }}
{{- printf "%s-web-letsencrypt" (include "henry-angular-chart.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate web hostname
*/}}
{{- define "henry-angular-chart.web-hostname" }}
{{- if (and .Values.config.web.hostname (not (empty .Values.config.web.hostname))) }}
{{- printf .Values.config.web.hostname }}
{{- else }}
{{- if .Values.ingress.web.enabled }}
{{- printf .Values.ingress.web.hostname }}
{{- else }}
{{- printf "%s-web" (include "henry-angular-chart.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate web base url
*/}}
{{- define "henry-angular-chart.web-base-url" }}
{{- if (and .Values.config.web.baseUrl (not (empty .Values.config.web.baseUrl))) }}
{{- printf .Values.config.web.baseUrl }}
{{- else }}
{{- if .Values.ingress.web.enabled }}
{{- $hostname := ((empty (include "henry-angular-chart.web-hostname" .)) | ternary .Values.ingress.web.hostname (include "henry-angular-chart.web-hostname" .)) }}
{{- $protocol := (.Values.ingress.web.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "henry-angular-chart.web-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate api certificate
*/}}
{{- define "henry-angular-chart.api-certificate" }}
{{- if (not (empty .Values.ingress.api.certificate)) }}
{{- printf .Values.ingress.api.certificate }}
{{- else }}
{{- printf "%s-api-letsencrypt" (include "henry-angular-chart.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate api hostname
*/}}
{{- define "henry-angular-chart.api-hostname" }}
{{- if (and .Values.config.api.hostname (not (empty .Values.config.api.hostname))) }}
{{- printf .Values.config.api.hostname }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- printf .Values.ingress.api.hostname }}
{{- else }}
{{- printf "%s-api" (include "henry-angular-chart.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate api base url
*/}}
{{- define "henry-angular-chart.api-base-url" }}
{{- if (and .Values.config.api.baseUrl (not (empty .Values.config.api.baseUrl))) }}
{{- printf .Values.config.api.baseUrl }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- $hostname := ((empty (include "henry-angular-chart.api-hostname" .)) | ternary .Values.ingress.api.hostname (include "henry-angular-chart.api-hostname" .)) }}
{{- $protocol := (.Values.ingress.api.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "henry-angular-chart.api-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
