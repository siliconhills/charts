apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "directus.fullname" . }}-directus
  labels:
    app: {{ template "directus.name" . }}-directus
    chart: {{ .Chart.Name }}-{{ .Chart.Version }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  selector:
    matchLabels:
      app: {{ template "directus.name" . }}-directus
      release: {{ .Release.Name }}
  strategy:
    type: {{ .Values.config.updateStrategy }}
  template:
    metadata:
      labels:
        app: {{ template "directus.name" . }}-directus
        release: {{ .Release.Name }}
      {{- if .Values.persistence.velero.enabled }}
      annotations:
        backup.velero.io/backup-volumes: data
      {{- end }}
    spec:
      affinity:
        nodeAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: {{ .Values.persistence.enabled | ternary "91" "89" }}
              preference:
                matchExpressions:
                  - key: application/state
                    operator: In
                    values:
                      - {{ .Values.persistence.enabled | ternary "stateful" "stateless" }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 90
              podAffinityTerm:
                topologyKey: kubernetes.io/hostname
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "directus.name" . }}-directus
      containers:
        - name: {{ template "directus.fullname" . }}-directus
          image: {{ .Values.images.directus.repository }}:{{ .Values.images.directus.tag }}
          imagePullPolicy: {{ .Values.config.imagePullPolicy }}
{{- if .Values.config.directus.resources.enabled }}
          resources:
            requests:
{{ toYaml .Values.config.directus.resources.requests | indent 14 }}
            limits:
{{ toYaml .Values.config.directus.resources.limits | indent 14 }}
{{- end }}
          ports:
            - name: container
              containerPort: 8055
          volumeMounts:
            - name: data
              mountPath: /_data
            - name: data
              mountPath: /var/directus/public/uploads
              subPath: uploads
          env:
            - name: DB_CLIENT
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_client
            - name: KEY
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: auth_public_key
            - name: SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: auth_secret_key
            - name: DB_HOST
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_host
            - name: DB_PORT
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_port
            - name: DB_DATABASE
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_database
            - name: DB_USER
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_username
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_password
            - name: DB_CONNECTION_STRING
              valueFrom:
                secretKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_url
            - name: ADMIN_EMAIL
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: admin_email
            - name: ADMIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: admin_password
            - name: DB_SSL__REJECT_UNAUTHORIZED
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_ssl_reject_unauthorized
            - name: DB_SSL__CA
              valueFrom:
                secretKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: db_certificate
            - name: EMAIL_FROM
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_from
            - name: EMAIL_TRANSPORT
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_transport
            - name: EMAIL_SENDMAIL_NEW_LINE
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_sendmail_new_line
            - name: EMAIL_SENDMAIL_PATH
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_sendmail_path
            - name: EMAIL_MAILGUN_API_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_mailgun_api_key
            - name: EMAIL_MAILGUN_DOMAIN
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_mailgun_domain
            - name: PUBLIC_URL
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: public_url
            - name: EMAIL_SMTP_HOST
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_smtp_host
            - name: EMAIL_SMTP_PORT
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_smtp_port
            - name: EMAIL_SMTP_USER
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_smtp_user
            - name: EMAIL_SMTP_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_smtp_password
            - name: EMAIL_SMTP_POOL
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_smtp_pool
            - name: EMAIL_SMTP_SECURE
              valueFrom:
                configMapKeyRef:
                  name: {{ template "directus.fullname" . }}
                  key: email_smtp_secure
          livenessProbe:
            httpGet:
              path: /server/ping
              port: container
{{ toYaml .Values.probes.liveness | indent 12 }}
          readinessProbe:
            httpGet:
              path: /server/ping
              port: container
{{ toYaml .Values.probes.readiness | indent 12 }}
      volumes:
        - name: data
          {{- if .Values.persistence.enabled }}
          persistentVolumeClaim:
            claimName: {{ .Values.persistence.existingClaim.directus | default (printf "%s-directus" (include "directus.fullname" . )) }}
          {{- else }}
          emptyDir: {}
          {{- end }}
