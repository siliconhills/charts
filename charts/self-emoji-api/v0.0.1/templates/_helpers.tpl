{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "self-emoji-api.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "self-emoji-api.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate self emoji api certificate
*/}}
{{- define "self-emoji-api.self-emoji-api-certificate" }}
{{- if (not (empty .Values.ingress.selfEmojiApi.certificate)) }}
{{- printf .Values.ingress.selfEmojiApi.certificate }}
{{- else }}
{{- printf "%s-self-emoji-api-letsencrypt" (include "self-emoji-api.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate mongo express certificate
*/}}
{{- define "self-emoji-api.mongo-express-certificate" }}
{{- if (not (empty .Values.ingress.mongoExpress.certificate)) }}
{{- printf .Values.ingress.mongoExpress.certificate }}
{{- else }}
{{- printf "%s-mongo-express-letsencrypt" (include "self-emoji-api.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate self emoji api hostname
*/}}
{{- define "self-emoji-api.self-emoji-api-hostname" }}
{{- if (and .Values.config.selfEmojiApi.hostname (not (empty .Values.config.selfEmojiApi.hostname))) }}
{{- printf .Values.config.selfEmojiApi.hostname }}
{{- else }}
{{- if .Values.ingress.selfEmojiApi.enabled }}
{{- printf .Values.ingress.selfEmojiApi.hostname }}
{{- else }}
{{- printf "%s-self-emoji-api" (include "self-emoji-api.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate self emoji api base url
*/}}
{{- define "self-emoji-api.self-emoji-api-base-url" }}
{{- if (and .Values.config.selfEmojiApi.baseUrl (not (empty .Values.config.selfEmojiApi.baseUrl))) }}
{{- printf .Values.config.selfEmojiApi.baseUrl }}
{{- else }}
{{- if .Values.ingress.selfEmojiApi.enabled }}
{{- $hostname := ((empty (include "self-emoji-api.self-emoji-api-hostname" .)) | ternary .Values.ingress.selfEmojiApi.hostname (include "self-emoji-api.self-emoji-api-hostname" .)) }}
{{- $path := (eq .Values.ingress.selfEmojiApi.path "/" | ternary "" .Values.ingress.selfEmojiApi.path) }}
{{- $protocol := (.Values.ingress.selfEmojiApi.tls | ternary "https" "http") }}
{{- printf "%s://%s%s" $protocol $hostname $path }}
{{- else }}
{{- printf "http://%s" (include "self-emoji-api.self-emoji-api-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate mongodb server
*/}}
{{- define "self-emoji-api.mongodb-server" }}
{{- if .Values.config.mongodb.replicaSet.enabled }}
{{- printf "%s-mongodb-0.%s-mongodb-gvr.%s.svc,%s-mongodb-1.%s-mongodb-gvr.%s.svc,%s-mongodb-2.%s-mongodb-gvr.%s.svc" (include "self-emoji-api.fullname" .) (include "self-emoji-api.fullname" .) .Release.Namespace (include "self-emoji-api.fullname" .) (include "self-emoji-api.fullname" .) .Release.Namespace (include "self-emoji-api.fullname" .) (include "self-emoji-api.fullname" .) .Release.Namespace }}
{{- else }}
{{- printf "%s-mongodb" (include "self-emoji-api.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate mongodb url
*/}}
{{- define "self-emoji-api.mongodb-url" }}
{{- $mongodb := .Values.config.mongodb }}
{{- if $mongodb.internal }}
{{- printf "mongodb://%s-mongodb:27017/%s" (include "self-emoji-api.fullname" .) $mongodb.database }}
{{- else }}
{{- if $mongodb.url }}
{{- printf $mongodb.url }}
{{- else }}
{{- $credentials := (empty $mongodb.username | ternary "" (printf "%s:%s" $mongodb.username $mongodb.password)) }}
{{- printf "mongodb://%s@%s:%s/%s" $credentials $mongodb.host $mongodb.port $mongodb.database }}
{{- end }}
{{- end }}
{{- end }}
