{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "fineart-payment.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "fineart-payment.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate app certificate
*/}}
{{- define "fineart-payment.app-certificate" }}
{{- if (not (empty .Values.ingress.app.certificate)) }}
{{- printf .Values.ingress.app.certificate }}
{{- else }}
{{- printf "%s-app-letsencrypt" (include "fineart-payment.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate app hostname
*/}}
{{- define "fineart-payment.app-hostname" }}
{{- if (and .Values.config.app.hostname (not (empty .Values.config.app.hostname))) }}
{{- printf .Values.config.app.hostname }}
{{- else }}
{{- if .Values.ingress.app.enabled }}
{{- printf .Values.ingress.app.hostname }}
{{- else }}
{{- printf "%s-app" (include "fineart-payment.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate app base url
*/}}
{{- define "fineart-payment.app-base-url" }}
{{- if (and .Values.config.app.baseUrl (not (empty .Values.config.app.baseUrl))) }}
{{- printf .Values.config.app.baseUrl }}
{{- else }}
{{- if .Values.ingress.app.enabled }}
{{- $hostname := ((empty (include "fineart-payment.app-hostname" .)) | ternary .Values.ingress.app.hostname (include "fineart-payment.app-hostname" .)) }}
{{- $protocol := (.Values.ingress.app.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "fineart-payment.app-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
