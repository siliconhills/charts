{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "nuevehrm.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "nuevehrm.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate backend certificate
*/}}
{{- define "nuevehrm.backend-certificate" }}
{{- if (not (empty .Values.ingress.backend.certificate)) }}
{{- printf .Values.ingress.backend.certificate }}
{{- else }}
{{- printf "%s-backend-letsencrypt" (include "nuevehrm.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate backend hostname
*/}}
{{- define "nuevehrm.backend-hostname" }}
{{- if (and .Values.config.backend.hostname (not (empty .Values.config.backend.hostname))) }}
{{- printf .Values.config.backend.hostname }}
{{- else }}
{{- if .Values.ingress.backend.enabled }}
{{- printf .Values.ingress.backend.hostname }}
{{- else }}
{{- printf "%s-backend" (include "nuevehrm.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate backend base url
*/}}
{{- define "nuevehrm.backend-base-url" }}
{{- if (and .Values.config.backend.baseUrl (not (empty .Values.config.backend.baseUrl))) }}
{{- printf .Values.config.backend.baseUrl }}
{{- else }}
{{- if .Values.ingress.backend.enabled }}
{{- $hostname := ((empty (include "nuevehrm.backend-hostname" .)) | ternary .Values.ingress.backend.hostname (include "nuevehrm.backend-hostname" .)) }}
{{- $protocol := (.Values.ingress.backend.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "nuevehrm.backend-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate frontend certificate
*/}}
{{- define "nuevehrm.frontend-certificate" }}
{{- if (not (empty .Values.ingress.frontend.certificate)) }}
{{- printf .Values.ingress.frontend.certificate }}
{{- else }}
{{- printf "%s-frontend-letsencrypt" (include "nuevehrm.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate frontend hostname
*/}}
{{- define "nuevehrm.frontend-hostname" }}
{{- if (and .Values.config.frontend.hostname (not (empty .Values.config.frontend.hostname))) }}
{{- printf .Values.config.frontend.hostname }}
{{- else }}
{{- if .Values.ingress.frontend.enabled }}
{{- printf .Values.ingress.frontend.hostname }}
{{- else }}
{{- printf "%s-frontend" (include "nuevehrm.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate frontend base url
*/}}
{{- define "nuevehrm.frontend-base-url" }}
{{- if (and .Values.config.frontend.baseUrl (not (empty .Values.config.frontend.baseUrl))) }}
{{- printf .Values.config.frontend.baseUrl }}
{{- else }}
{{- if .Values.ingress.frontend.enabled }}
{{- $hostname := ((empty (include "nuevehrm.frontend-hostname" .)) | ternary .Values.ingress.frontend.hostname (include "nuevehrm.frontend-hostname" .)) }}
{{- $protocol := (.Values.ingress.frontend.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "nuevehrm.frontend-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
